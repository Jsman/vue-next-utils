export function isObject(obj: object): void {
  const rules = [Array.isArray(obj), !(obj instanceof Object)]
  if (rules.includes(true)) throw new Error(`${obj}不是object`)
}
