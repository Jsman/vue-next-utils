/*
 * @Author       : 邱狮杰
 * @Date         : 2021-08-12 10:26:00
 * @LastEditTime : 2021-08-12 15:32:30
 * @FilePath     : /vue-next-utils/src/index.d.ts
 * @Description  :
 */
import { WatchSource, ComputedRef, Ref, UnwrapRef } from 'vue'
import { App } from '@vue/runtime-dom'

declare class Bus {
  eventList: Map<string, Function[]>
  // 发布
  emit<T extends unknown>(key: string, val: T): void
  // 订阅
  on<T>(key: string, cb: (res: T) => void): void
  // 销毁
  off(key: string): void
}

export type MultiWatchSources = (WatchSource<unknown> | object)[]
export type baseCb = () => void
export type withDestroyCb = () => () => void

export declare function useEffect(cb: baseCb): void
export declare function useEffect(cb: baseCb | withDestroyCb): void
export declare function useEffect(cb: withDestroyCb): void
export declare function useEffect(cb: baseCb | withDestroyCb, deps?: []): void
export declare function useEffect(cb: baseCb, deps?: MultiWatchSources[]): void

export declare function useBus(): Bus

export declare function setupBus(app: App): void

export declare function setupImageLazyLoading(app: App): void

export declare function useDestroyBus(destroyKey: string[]): boolean | never
export declare function useDestroyBus(destroyKey: string): boolean | never
export declare function useDestroyBus(destroyKey: unknown): boolean | never

export declare function useDestroyBusOnUnmounted(destroyKey: string[]): void | never
export declare function useDestroyBusOnUnmounted(destroyKey: string): void | never
export declare function useDestroyBusOnUnmounted(destroyKey: unknown): void | never

export declare function setupScrollScreen(app: App): void

export interface scrollScreenOptionsTypes {
  left: number
  top: number
  run: boolean
}
export type Cb<T> = () => T

export declare function useMemo<T>(cb: Cb<T>): ComputedRef<T>

type setFn<T> = (val: UnwrapRef<T>) => UnwrapRef<T>

export declare function useState<T>(cb?: T | undefined): [Ref<UnwrapRef<T> | null>, (val: any | setFn<T>) => void]
type NotUndefined<T> = T extends undefined ? never : T

type PropsWithDefaults<Base, Defaults> = Base &
  {
    [K in keyof Defaults]: K extends keyof Base ? NotUndefined<Base[K]> : never
  }

type Constructor =
  | MapConstructor
  | SetConstructor
  | DateConstructor
  | ArrayConstructor
  | ErrorConstructor
  | ProxyConstructor
  | BigIntConstructor
  | NumberConstructor
  | ObjectConstructor
  | RegExpConstructor
  | SymbolConstructor
  | StringConstructor
  | BooleanConstructor
  | PromiseConstructor
  | AsyncGeneratorFunctionConstructor
  | SharedArrayBufferConstructor
  | GeneratorFunctionConstructor
  | Uint8ArrayConstructor
  | Uint16ArrayConstructor
  | Uint32ArrayConstructor
  | Uint8ClampedArrayConstructor
  | ReferenceErrorConstructor
  | BigUint64ArrayConstructor
  | CustomElementConstructor
  | Float32ArrayConstructor
  | Float64ArrayConstructor
  | SyntaxErrorConstructor
  | WeakMapConstructor
  | WeakSetConstructor
  | DataViewConstructor
  | FunctionConstructor
  | URIErrorConstructor
  | EvalErrorConstructor
  | Int8ArrayConstructor
  | TypeErrorConstructor
  | Int16ArrayConstructor
  | Int32ArrayConstructor
  | RangeErrorConstructor
  | ArrayBufferConstructor

interface basePropsDefaultTypes<T> {
  type?: Constructor
  require?: boolean
  default?: (() => T extends Constructor ? Constructor : T) | T
  validator?: (val: T) => boolean
}

type property<T> = {
  [key in keyof T]: basePropsDefaultTypes<T>
}

export declare function useDefineProps<T extends object>(props: T | property<T>): PropsWithDefaults<Readonly<unknown>, T | property<T>>
