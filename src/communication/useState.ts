import { Ref, ref, UnwrapRef } from 'vue'
type setFn<T> = (val: UnwrapRef<T>) => UnwrapRef<T>

export function useState<T>(cb?: T): [Ref<UnwrapRef<T | null>>, (val: any | setFn<T>) => void] {
  const val: Ref<UnwrapRef<T | null>> = ref<T | null>(cb ? cb : null)
  function handleSetState(state: any | setFn<T>): void {
    if (typeof state === 'function') {
      val.value = state(val.value)
    } else val.value = state
  }
  return [val, handleSetState]
}
