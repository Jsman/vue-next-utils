import { getCurrentInstance } from 'vue'
import type { ConfigurationOptions } from '../config/configurationConst'
import { CONFIGURATION_OPTIONS } from '../config/configurationConst'

export function useVueNextUtilsConfig(): ConfigurationOptions {
  const config = getCurrentInstance()?.appContext.config.globalProperties[CONFIGURATION_OPTIONS]
  return config
}
