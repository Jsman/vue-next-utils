import { computed, ComputedRef } from 'vue'

type Cb<T> = () => T

export function useMemo<T>(cb: Cb<T>): ComputedRef<T> {
  return computed(cb)
}
