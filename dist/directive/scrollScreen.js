"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.scrollScreen = void 0;
var object_1 = require("../utils/object");
function scrollRun(options) {
    window.scrollTo(__assign(__assign({}, options), { behavior: 'smooth' }));
}
var scrollScreen = {
    mounted: function (el, bind) {
        var _a;
        object_1.isObject(bind.value);
        ((_a = bind.value) === null || _a === void 0 ? void 0 : _a.run) && scrollRun(bind.value);
    },
    updated: function (el, bind) {
        var _a;
        object_1.isObject(bind.value);
        ((_a = bind.value) === null || _a === void 0 ? void 0 : _a.run) && scrollRun(bind.value);
    },
};
exports.scrollScreen = scrollScreen;
