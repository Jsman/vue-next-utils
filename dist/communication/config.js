"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useVueNextUtilsConfig = void 0;
var vue_1 = require("vue");
var configurationConst_1 = require("../config/configurationConst");
function useVueNextUtilsConfig() {
    var _a;
    var config = (_a = vue_1.getCurrentInstance()) === null || _a === void 0 ? void 0 : _a.appContext.config.globalProperties[configurationConst_1.CONFIGURATION_OPTIONS];
    return config;
}
exports.useVueNextUtilsConfig = useVueNextUtilsConfig;
