"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useState = void 0;
var vue_1 = require("vue");
function useState(cb) {
    var val = vue_1.ref(cb ? cb : null);
    function handleSetState(state) {
        if (typeof state === 'function') {
            val.value = state(val.value);
        }
        else
            val.value = state;
    }
    return [val, handleSetState];
}
exports.useState = useState;
