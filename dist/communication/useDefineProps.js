"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useDefineProps = void 0;
var vue_1 = require("vue");
function useDefineProps(props) {
    return vue_1.withDefaults(vue_1.defineProps(), props);
}
exports.useDefineProps = useDefineProps;
