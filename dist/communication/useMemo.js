"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useMemo = void 0;
var vue_1 = require("vue");
function useMemo(cb) {
    return vue_1.computed(cb);
}
exports.useMemo = useMemo;
