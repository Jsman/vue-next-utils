"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isObject = void 0;
function isObject(obj) {
    var rules = [Array.isArray(obj), !(obj instanceof Object)];
    if (rules.includes(true))
        throw new Error(obj + "\u4E0D\u662Fobject");
}
exports.isObject = isObject;
